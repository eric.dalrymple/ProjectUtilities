This project is to help me speed up the process of setting myself up on a new or
freshly formatted machine. I plan to mostly use this repo to store exported
settings files for various IDEs (e.g. code formatting profile for Eclipse,
Xcode project configurations, etc.).